import subprocess as sp


for x in range(1 , 28, 1):
    o_file=("CONFIG_"+str(x))
    F=open(o_file, "w")
    F.write("coverage=/home/user/Asztal/Louvain-FLs/Feri-matrix/joda-time/joda-time."+str(x)+"b.method.cov.SoDA.csv\n")
    F.write("results=/home/user/Asztal/Louvain-FLs/Feri-matrix/joda-time/joda-time."+str(x)+"b.res.SoDA.csv\n")
    F.write("change=/home/user/Asztal/Louvain-FLs/Feri-matrix/joda-time/joda-time-changes.csv\n")
    F.write("map=/home/user/Asztal/Louvain-FLs/Feri-matrix/joda-time/joda-time."+str(x)+"b.traces.names.txt\n")
    F.write("bugID="+str(x)+"\n")
    F.write("output=/home/user/Asztal/Louvain-FLs/Feri-matrix/joda-time/joda-time."+str(x)+"-results.csv\n")
    F.close()

    cmd = "python3 main.py -cF "+"CONFIG_"+str(x)
    sp.call(cmd, shell=True)