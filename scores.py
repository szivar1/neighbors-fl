import FL_metrics
import NFL_metrics


class Metodusok():
    def __init__(self, metodusnevek):
        self.metodusnevek_lista = list(metodusnevek)
        self.metrika_lista = {}


    def score_szamitas(self, teszteredmenyek, graf):
        tarantula = FL_metrics.FL_Metric(self.metodusnevek_lista)
        ochiai = FL_metrics.FL_Metric(self.metodusnevek_lista)
        op2 = FL_metrics.FL_Metric(self.metodusnevek_lista)
        barinel = FL_metrics.FL_Metric(self.metodusnevek_lista)
        dstar = FL_metrics.FL_Metric(self.metodusnevek_lista)
        kulczynski = FL_metrics.FL_Metric(self.metodusnevek_lista)
        mcCon = FL_metrics.FL_Metric(self.metodusnevek_lista)
        jaccarrd = FL_metrics.FL_Metric(self.metodusnevek_lista)
        zoltar = FL_metrics.FL_Metric(self.metodusnevek_lista)
        minus = FL_metrics.FL_Metric(self.metodusnevek_lista)
        drt = FL_metrics.FL_Metric(self.metodusnevek_lista)
        wong3 = FL_metrics.FL_Metric(self.metodusnevek_lista)


        for metodus in self.metodusnevek_lista:
            fedett_tesztek = list(graf.neighbors(metodus))
            c_ef = teszteredmenyek.fedett_tesztek_kozott_failed(fedett_tesztek)
            c_nf = teszteredmenyek.number_of_failed-c_ef
            c_ep = teszteredmenyek.fedett_tesztek_kozott_pass(fedett_tesztek)
            c_np = teszteredmenyek.number_of_pass-c_ep

            tarantula.set_tarantula(c_ef, c_nf, c_ep, c_np, metodus)
            ochiai.set_ochiai(c_ef, c_nf, c_ep, c_np, metodus)
            op2.set_op2(c_ef, c_nf, c_ep, c_np, metodus)
            barinel.set_barinel(c_ef, c_nf, c_ep, c_np, metodus)
            dstar.set_dstar(c_ef, c_nf, c_ep, c_np, metodus)
            kulczynski.set_kulczynski(c_ef, c_nf, c_ep, c_np, metodus)
            mcCon.set_mcCon(c_ef, c_nf, c_ep, c_np, metodus)
            jaccarrd.set_jaccard(c_ef, c_nf, c_ep, c_np, metodus)
            zoltar.set_zoltar(c_ef, c_nf, c_ep, c_np, metodus)
            minus.set_minus(c_ef, c_nf, c_ep, c_np, metodus)
            drt.set_drt(c_ef, c_nf, c_ep, c_np, metodus)
            wong3.set_wong3(c_ef, c_nf, c_ep, c_np, metodus)


        # ========= NFL metrics calc =======
        buko_tesztek = teszteredmenyek.get_buko_tesztek()

        NFL_1 = NFL_metrics.NFL_Metric(self.metodusnevek_lista)
        NFL_1.neighborsFL_1(graf, self.metodusnevek_lista, buko_tesztek)

        NFL_2 = NFL_metrics.NFL_Metric(self.metodusnevek_lista)
        NFL_2.neighborsFL_2(graf, self.metodusnevek_lista, buko_tesztek)

        NFL_3 = NFL_metrics.NFL_Metric(self.metodusnevek_lista)
        NFL_3.neighborsFL_3(graf, self.metodusnevek_lista, buko_tesztek)

        NFL_4 = NFL_metrics.NFL_Metric(self.metodusnevek_lista)
        NFL_4.neighborsFL_4(graf, self.metodusnevek_lista, buko_tesztek)

        NFL_5 = NFL_metrics.NFL_Metric(self.metodusnevek_lista)
        NFL_5.neighborsFL_5(graf, self.metodusnevek_lista, buko_tesztek)

        self.metrika_lista["Tarantula"] = tarantula
        self.metrika_lista["Ochiai"] = ochiai
        self.metrika_lista["Op2"] = op2
        self.metrika_lista["Barinel"] = barinel
        self.metrika_lista["DStar"] = dstar
        self.metrika_lista["Kulczynski"] = kulczynski
        self.metrika_lista["McCon"] = mcCon
        self.metrika_lista["Jaccard"] = jaccarrd
        self.metrika_lista["Zoltar"] = zoltar
        self.metrika_lista["Minus"] = minus
        self.metrika_lista["DRT"] = drt
        self.metrika_lista["Wong3"] = wong3
        self.metrika_lista["NFL-1"] = NFL_1
        self.metrika_lista["NFL-2"] = NFL_2
        self.metrika_lista["NFL-3"] = NFL_3
        self.metrika_lista["NFL-4"] = NFL_4
        self.metrika_lista["NFL-5"] = NFL_5

