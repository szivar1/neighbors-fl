import math

class FL_Metric:
    def __init__(self, metodusnev):
        self.score = self._score_init(metodusnev)


    def _score_init(self, metodusnev):
        _score = {}
        for metodus in metodusnev:
            _score[metodus] = 0
        return _score


    def set_tarantula(self, c_ef, c_nf, c_ep, c_np, metodus):
        if c_ef + c_nf == 0:
            self.score[metodus] = 0.0
            return
        szamlalo = float(c_ef)/float(c_ef + c_nf)

        nevezo1 = float(c_ef) / (float(c_ef + c_nf))
        nevezo2 = 0.0
        if c_ep + c_np > 0:
            nevezo2 = float(c_ep) / (float(c_ep + c_np))
        nevezo = float(nevezo1)+float(nevezo2)

        if szamlalo==0.0:
            self.score[metodus] = 0.0
            return
        if nevezo == 0.0:
            self.score[metodus] = 0.0
            return
        self.score[metodus] = float(szamlalo)/float(nevezo)
        return


    def set_ochiai(self, c_ef, c_nf, c_ep, c_np, metodus):
        szamlalo = float(c_ef)
        nevezo = math.sqrt( float(c_ef + c_nf)*float(c_ef + c_ep) )

        if szamlalo==0.0:
            self.score[metodus] = 0.0
            return
        if nevezo == 0.0:
            self.score[metodus] = 0.0
            return
        self.score[metodus] = float(szamlalo)/float(nevezo)
        return


    def set_op2(self, c_ef, c_nf, c_ep, c_np, metodus):
        self.score[metodus] = float(c_ef) - float(float(c_ep)/(float(c_ep + c_np+1)))
        return


    def set_barinel(self, c_ef, c_nf, c_ep, c_np, metodus):
        if float(c_ep)+float(c_ef) > 0.0:
            self.score[metodus] =  1.0 - float( float(c_ep) / float(float(c_ep)+float(c_ef)))
        else:
            self.score[metodus] =  0.0
        return


    def set_dstar(self, c_ef, c_nf, c_ep, c_np, metodus):
        szamlalo = c_ef*c_ef
        nevezo = c_nf+c_ep

        if szamlalo==0.0:
            self.score[metodus] = 0.0
            return
        if nevezo == 0.0:
            self.score[metodus] = 0.0
            return
        self.score[metodus] = float(szamlalo)/float(nevezo)
        return


    def set_kulczynski(self, c_ef, c_nf, c_ep, c_np, metodus):
        if c_ef == 0.0 or  c_ef + c_nf == 0.0 or c_ef+c_ep==0.0:
            self.score[metodus] = 0.0
            return

        tag1 = float(c_ef)/float(c_ef+c_nf)
        tag2 = float(c_ef)/float(c_ef+c_ep)
        self.score[metodus] = 0.5*(float(tag1)+float(tag2))
        return


    def set_mcCon(self, c_ef, c_nf, c_ep, c_np, metodus):
        if c_ef+c_nf == 0.0 or c_ef+c_ep==0.0:
            self.score[metodus] = 0.0
            return
        if c_ef*c_ef - (c_np*c_ep) == 0.0:
            self.score[metodus] = 0.0
            return
        szamlalo = float(c_ef*c_ef - (c_np*c_ep))
        nevezo = float(c_ef+c_nf)*float(c_ef+c_ep)
        self.score[metodus] = float(szamlalo)/float(nevezo)
        return


    def set_jaccard(self, c_ef, c_nf, c_ep, c_np, metodus):
        if c_ef == 0.0 or c_ef+c_ep+c_nf==0.0:
            self.score[metodus] = 0.0
            return
        self.score[metodus] = float(c_ef)/float( c_ef + c_nf + c_ep)
        return



    def set_zoltar(self, c_ef, c_nf, c_ep, c_np, metodus):
        szamlalo = c_ef
        nevezo = float(c_ef + c_nf + c_ep)
        if c_ef != 0.0:
            nevezo = nevezo + float(10000*(c_nf*c_ep))/float(c_ef)



        if szamlalo == 0.0:
            self.score[metodus] = 0.0
            return

        if nevezo == 0.0:
            self.score[metodus] = 0.0
            return
        self.score[metodus] = float(szamlalo)/float(nevezo)
        return


    def set_minus(self, c_ef, c_nf, c_ep, c_np, metodus):
        szamlalo1 = float(c_ef) / float((c_ef + c_nf))
        nevezo1 = float(c_ef)/float((c_ef+c_nf)) + float(c_ep)/float((c_ep+c_np))

        szamlalo2 = 1.0 - szamlalo1
        nevezo2 = 1.0 - float(c_ef)/float((c_ef+c_nf)) + 1.0 - float(c_ep)/float((c_ep+c_np))

        if nevezo1==0.0 and nevezo2==0.0:
            self.score[metodus] = 0.0
            return

        self.score[metodus] = float(szamlalo1)/float(nevezo1) - float(szamlalo2)/float(nevezo2)


    def set_drt(self, c_ef, c_nf, c_ep, c_np, metodus):
        if c_ef == 0.0:
            self.score[metodus] = 0.0
            return
        self.score[metodus] = float(c_ef)/ float(1.0 + float( float(c_ep) / float( c_ef + c_nf + c_ep + c_np)))
        return


    def set_wong3(self, c_ef, c_nf, c_ep, c_np, metodus):
        if c_ep <= 2.0:
            self.score[metodus] = c_ef - c_ep
            return
        if c_ep > 2.0 and 10 >= c_ep:
            self.score[metodus] = c_ef - 2.0 + 0.1*float(c_ep-2.0)
            return
        if 10 < c_ep:
            self.score[metodus] = c_ef - 2.8 + 0.001*float(c_ep-10.0)
            return