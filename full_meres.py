import subprocess as sp

cmd_str = "python3 main.py"


"""
for x in range(1, 28):
    cmd_str = cmd_str+" --coverage=/home/user/Asztal/Louvain-FLs/Feri-matrix/joda-time/joda-time."+str(x)+"b.method.cov.SoDA.csv"
    cmd_str = cmd_str+" --results=/home/user/Asztal/Louvain-FLs/Feri-matrix/joda-time/joda-time."+str(x)+"b.res.SoDA.csv"
    cmd_str = cmd_str+" --change=/home/user/Asztal/Louvain-FLs/Feri-matrix/joda-time/joda-time-changes.csv"
    cmd_str = cmd_str+" --map=/home/user/Asztal/Louvain-FLs/Feri-matrix/joda-time/joda-time."+str(x)+"b.traces.names.txt"
    cmd_str = cmd_str+" --bugID="+str(x)
    cmd_str = cmd_str+" --output=/home/user/Asztal/Louvain-FLs/Feri-matrix/joda-time/joda-time."+str(x)+"-results.csv"
    sp.call(cmd_str, shell=True)
"""

"""
for x in range(1, 54):
    cmd_str = cmd_str+" --coverage=/home/user/Asztal/Louvain-FLs/Feri-matrix/commons-lang/commons-lang."+str(x)+"b.method.cov.SoDA.csv"
    cmd_str = cmd_str+" --results=/home/user/Asztal/Louvain-FLs/Feri-matrix/commons-lang/commons-lang."+str(x)+"b.res.SoDA.csv"
    cmd_str = cmd_str+" --change=/home/user/Asztal/Louvain-FLs/Feri-matrix/commons-lang/commons-lang-changes.csv"
    cmd_str = cmd_str+" --map=/home/user/Asztal/Louvain-FLs/Feri-matrix/commons-lang/commons-lang."+str(x)+"b.traces.names.txt"
    cmd_str = cmd_str+" --bugID="+str(x)
    cmd_str = cmd_str+" --output=/home/user/Asztal/Louvain-FLs/Feri-matrix/commons-lang/commons-lang."+str(x)+"-results.csv"
    sp.call(cmd_str, shell=True)
"""

"""
for x in range(1, 105):
    cmd_str = cmd_str+" --coverage=/home/user/Asztal/Louvain-FLs/Feri-matrix/commons-math/commons-math."+str(x)+"b.method.cov.SoDA.csv"
    cmd_str = cmd_str+" --results=/home/user/Asztal/Louvain-FLs/Feri-matrix/commons-math/commons-math."+str(x)+"b.res.SoDA.csv"
    cmd_str = cmd_str+" --change=/home/user/Asztal/Louvain-FLs/Feri-matrix/commons-math/commons-math-changes.csv"
    cmd_str = cmd_str+" --map=/home/user/Asztal/Louvain-FLs/Feri-matrix/commons-math/commons-math."+str(x)+"b.traces.names.txt"
    cmd_str = cmd_str+" --bugID="+str(x)
    cmd_str = cmd_str+" --output=/home/user/Asztal/Louvain-FLs/Feri-matrix/commons-math/commons-math."+str(x)+"-results.csv"
    sp.call(cmd_str, shell=True)
"""

for x in range(1, 27):
    cmd_str = cmd_str+" --coverage=/home/user/Asztal/Louvain-FLs/Feri-matrix/jfreechart/jfreechart."+str(x)+"b.method.cov.SoDA.csv"
    cmd_str = cmd_str+" --results=/home/user/Asztal/Louvain-FLs/Feri-matrix/jfreechart/jfreechart."+str(x)+"b.res.SoDA.csv"
    cmd_str = cmd_str+" --change=/home/user/Asztal/Louvain-FLs/Feri-matrix/jfreechart/jfreechart-changes.csv"
    cmd_str = cmd_str+" --map=/home/user/Asztal/Louvain-FLs/Feri-matrix/jfreechart/jfreechart."+str(x)+"b.traces.names.txt"
    cmd_str = cmd_str+" --bugID="+str(x)
    cmd_str = cmd_str+" --output=/home/user/Asztal/Louvain-FLs/Feri-matrix/jfreechart/jfreechart."+str(x)+"-results.csv"
    sp.call(cmd_str, shell=True)