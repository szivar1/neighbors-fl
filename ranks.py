import scipy.stats as ss


class Ranks():
    def __init__(self, metodusnevek):
        self.tarantula = self.init_metodus_ranks_dict(metodusnevek)
        self.ochiai = self.init_metodus_ranks_dict(metodusnevek)
        self.op2 = self.init_metodus_ranks_dict(metodusnevek)
        self.barinel = self.init_metodus_ranks_dict(metodusnevek)
        self.dstar = self.init_metodus_ranks_dict(metodusnevek)
        self.kulczynski = self.init_metodus_ranks_dict(metodusnevek)
        self.mcCon = self.init_metodus_ranks_dict(metodusnevek)
        self.jaccard = self.init_metodus_ranks_dict(metodusnevek)
        self.zoltar = self.init_metodus_ranks_dict(metodusnevek)
        self.minus = self.init_metodus_ranks_dict(metodusnevek)
        self.drt = self.init_metodus_ranks_dict(metodusnevek)
        self.wong3 = self.init_metodus_ranks_dict(metodusnevek)
        self.nfl_1 = self.init_metodus_ranks_dict(metodusnevek)
        self.nfl_2 = self.init_metodus_ranks_dict(metodusnevek)
        self.nfl_3 = self.init_metodus_ranks_dict(metodusnevek)
        self.nfl_4 = self.init_metodus_ranks_dict(metodusnevek)

    def init_metodus_ranks_dict(self, metodusnevek):
        _metodus_rank_dict = {}
        for m in metodusnevek:
            _metodus_rank_dict[m] = 0
        return _metodus_rank_dict



    def all_rank_calc(self, metodusok_metrikaLista):
        self.tarantula = self._rank_calc(metodusok_metrikaLista["Tarantula"].score)
        self.ochiai = self._rank_calc(metodusok_metrikaLista["Ochiai"].score)
        self.op2 = self._rank_calc(metodusok_metrikaLista["Op2"].score)
        self.barinel = self._rank_calc(metodusok_metrikaLista["Barinel"].score)
        self.dstar = self._rank_calc(metodusok_metrikaLista["DStar"].score)
        self.kulczynski = self._rank_calc(metodusok_metrikaLista["Kulczynski"].score)
        self.mcCon = self._rank_calc(metodusok_metrikaLista["McCon"].score)
        self.jaccard = self._rank_calc(metodusok_metrikaLista["Jaccard"].score)
        self.zoltar = self._rank_calc(metodusok_metrikaLista["Zoltar"].score)
        self.minus = self._rank_calc(metodusok_metrikaLista["Minus"].score)
        self.drt = self._rank_calc(metodusok_metrikaLista["DRT"].score)
        self.wong3 = self._rank_calc(metodusok_metrikaLista["Wong3"].score)
        self.nfl_1 = self._rank_calc(metodusok_metrikaLista["NFL-1"].score)
        self.nfl_2 = self._rank_calc(metodusok_metrikaLista["NFL-2"].score)
        self.nfl_3 = self._rank_calc(metodusok_metrikaLista["NFL-3"].score)
        self.nfl_4 = self._rank_calc(metodusok_metrikaLista["NFL-4"].score)
        self.nfl_5 = self._rank_calc(metodusok_metrikaLista["NFL-5"].score)


    def _rank_calc(self, metodus_score_dict):
        elem_vektor=[]
        tippeles_vektor=[]
        eredmeny = sorted(metodus_score_dict, key=metodus_score_dict.__getitem__, reverse=True)
        for elem in eredmeny:
            elem_vektor.append(elem)
            tippeles_vektor.append( abs(max(metodus_score_dict.values()) - metodus_score_dict[elem]))
        rank_vektor = ss.rankdata(tippeles_vektor,method='average')


        infection_ranks = {}
        for x in range(len(elem_vektor)):
            infection_ranks[elem_vektor[x]] = rank_vektor[x]

        return infection_ranks


    def print_min_ranks(self, hiba_helyei):
        #print("Tarantula;Ochiai;Op2;Barinel;DStar;Kulczynski;McCon;Jaccard;Zoltar;Minus;DRT;Wong3;NFL-1;NFL-2;NFL-3;NFL-4;NFL-5")
        print(str(min(self.get_hibahely_rank_list(hiba_helyei, self.tarantula)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.ochiai)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.op2)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.barinel)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.dstar)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.kulczynski)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.mcCon)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.jaccard)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.zoltar)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.minus)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.drt)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.wong3)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.nfl_1)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.nfl_2)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.nfl_3)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.nfl_4)))+";"+
              str(min(self.get_hibahely_rank_list(hiba_helyei, self.nfl_5)))+";"
              )


    def get_hibahely_rank_list(self, hiba_helyei, ranks):
        hiba_ranks = list()
        for hiba in hiba_helyei:
            hiba_ranks.append( ranks[hiba] )
        return hiba_ranks
